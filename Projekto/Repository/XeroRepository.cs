﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FindPrinter.Models;
using FindPrinter.Data;
using Projekto.Models;
using GeoCoordinatePortable;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using Projekto.Pakiet;

namespace FindPrinter.Repository
{
    public class XeroRepository : IXero
    {
        private readonly ProjektContext _context;

        public XeroRepository(ProjektContext context)
        {
            _context = context;
        }

        public void Add(Xero xero)
        {
            _context.Xero.Add(xero);
          
            _context.SaveChanges();
        }

        public Xero Find(long key)
        {
           return _context.Xero.FirstOrDefault(k => k.ID == key);
            
        }

        public IEnumerable<Xero> GetAll()
        {
            return _context.Xero.ToList();
        }

        public void Remove(long key)
        {
            var usun = _context.Xero.First(k => k.ID == key);
            _context.Xero.Remove(usun);
            _context.SaveChanges();
        }

        public void Update(Xero x)
        {
            _context.Xero.Update(x);
            _context.SaveChanges();
        }
        public List<PakietWynik> bliskie(KoordynatyFront kor)
        {
          GeoCoordinate cor = new GeoCoordinate(Double.Parse(kor.Szerokosc, CultureInfo.InvariantCulture), Double.Parse(kor.Wysokosc, CultureInfo.InvariantCulture));
          List<Koordynat> tmp = _context.Xero.Select(k => new Koordynat(k.ID,new GeoCoordinate(Double.Parse(k.Szerokosc, CultureInfo.InvariantCulture), Double.Parse(k.Wysokosc, CultureInfo.InvariantCulture)))).ToList<Koordynat>();
          List<PakietWynik> wynik = new List<PakietWynik>();
          for(int n=0;n< tmp.Count; n++)
          {
             double wartosc = tmp.ElementAt(n).geo.GetDistanceTo(cor);
            if (wartosc <= 5000)
            {
              
              wynik.Add(new PakietWynik { Xero=_context.Xero.Include(k => k.Opis).FirstOrDefault(k => k.ID == tmp.ElementAt(n).xeroId),
              odleglosc=wartosc
              });
            }
          }

      
      
      
          return wynik;
        }
        public void UsunWszystko()
    {
      List<Xero> x = _context.Xero.ToList();
      foreach (Xero xo in x)
      {
        _context.Xero.Remove(xo);
        _context.SaveChanges();
      }
    }

        public void dodajOpinie(Opinia o)
        {
          _context.Opinia.Add(o);
          _context.SaveChanges();
        }

        public List<Opinia> getOpinie(int id)
        {
      //   List<Opinia> opinie = new List<Opinia>();
      List<Opinia> opinie = new List<Opinia>();
      List<Opinia> tmp= _context.Opinia.Include("Xero").ToList();
     
      for(int n = 0; n < tmp.Count; n++)
      {
        
          if (tmp.ElementAt(n).xeroIdo == id)
          {
            opinie.Add(tmp.ElementAt(n));
          }
        
      }
      /*
          List<Xero> tmp=_context.Xero.Include("Opinia").ToList();
         
            foreach(Xero x in tmp){
                 foreach(Opinia o in x.Opinia)
                {
                    if (o.Xero.Nazwa == nazwa)
                    {
                        opinie.Add(o);
                    }
                }
              }
          
  */
      return opinie;
        }

     public List<Xero> getXeroUzytkownika(int id)
    {
      return  _context.Xero
        .Include(k=> k.Opis)
        .Include(k=> k.Opinia)
        .Where(m => m.ID == id).ToList();
    }
  }
}
