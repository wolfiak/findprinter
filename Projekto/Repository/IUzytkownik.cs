using FindPrinter.Models;
using Projekto.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Repository
{
    public interface IUzytkownik
    {
      void Add(Uzytkownik xero);
      IEnumerable<Uzytkownik> GetAll();
      Uzytkownik Find(long key);
      void Remove(long key);
      void Update(Uzytkownik x);
      Token Login(Uzytkownik uz);
  }
}
