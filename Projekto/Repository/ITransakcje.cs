using FindPrinter.Models;
using Microsoft.AspNetCore.Http;
using Projekto.Pakiet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Repository
{
    public interface ITransakcje
    {
       int Add(Transakcje transakcje);
       IEnumerable<Transakcje> GetAll();
       Transakcje Find(long key);
       void Remove(long key);
       void Update(UpdateTrans x);
       IEnumerable<Transakcje> FindAll(int id);
       void zmienStatus(PakietStatus pk);
       Task<PakietPlik>  ustalFormatICene(ICollection<IFormFile> files);
       ICollection<Transakcje> czasPlynie(int id);
  }
}
