using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FindPrinter.Models;
using FindPrinter.Data;
using Projekto.Auth;
using Newtonsoft.Json;

namespace Projekto.Repository
{
  public class UzytkownikRepository : IUzytkownik
  {

    private readonly ProjektContext context;
    private ITokenProvider tp;

    public UzytkownikRepository(ProjektContext context, ITokenProvider tp)
    {
      this.context = context;
      this.tp = tp;
    }

    public void Add(Uzytkownik uz)
    {
      context.Add(uz);
      context.SaveChanges();
    }

    public Uzytkownik Find(long key)
    {
      return context.Uzytkownik.FirstOrDefault(k => k.ID == key);
    }

    public IEnumerable<Uzytkownik> GetAll()
    {
      return context.Uzytkownik.ToList();
    }

    public Token Login(Uzytkownik uz)
    {
      var tmp = context.Uzytkownik.FirstOrDefault(u => u.Email == uz.Email && u.Password == uz.Password);
      if(tmp != null)
      {
        int minutes = 20;
        DateTime wygasa = DateTime.UtcNow.AddMinutes(minutes);
        var token = new Token
        {
          acess_token = tp.CreateToken(tmp, wygasa),
          expires_in = minutes * 60,
          id = tmp.ID
        };
        return token;
      }
      return null;

    }

    public void Remove(long key)
    {
      Uzytkownik u =context.Uzytkownik.First(k => k.ID == key);
      context.Uzytkownik.Remove(u);
      context.SaveChanges();
    }

    public void Update(Uzytkownik x)
    {
      context.Uzytkownik.Update(x);
      context.SaveChanges();
    }
  }
}
