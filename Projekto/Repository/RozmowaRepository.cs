using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekto.Models;
using FindPrinter.Data;
using Projekto.Pakiet;
using Microsoft.EntityFrameworkCore;

namespace Projekto.Repository
{
  public class RozmowaRepository : IRozmowa
  {
    private readonly ProjektContext context;

    public RozmowaRepository(ProjektContext context)
    {
      this.context = context;
    }
    public int Nowa(Rozmowa r)
    {

      //Console.WriteLine($"Rozmowa ID: {r.ID} \n xero: {r.Xero.ID} \n uzytkownik {r.Uzytkownik.ID}");
      bool wynik=context.Rozmowa.Any(k => k.UzytkownikID == r.UzytkownikID && k.XeroID == k.XeroID);
      if (wynik == true)
      {
        return context.Rozmowa.SingleOrDefault<Rozmowa>(ro => ro.XeroID == r.XeroID && ro.UzytkownikID == r.UzytkownikID).ID;
      }
      else
      {
        context.Rozmowa.Add(r);
        context.SaveChanges();
        return r.ID;
      }
    }
    public List<PakietRozmowy> mojeRozmowyXero(int id)
    {
      List<PakietRozmowy> rozmowy = new List<PakietRozmowy>();
      context.Rozmowa.Where(r => r.XeroID == id).ToList().ForEach(r =>
      {
        rozmowy.Add(new PakietRozmowy
        {
          Rozmowa= r,
          Uzytkownik = context.Uzytkownik.Single(u => u.ID == r.UzytkownikID),
          Xero = context.Xero.Single(x => x.ID == r.XeroID)
        });
      });

      return rozmowy;
    }

    public void NowaWiadomosc(Wiadomosc w)
    {
      context.Wiadomosc.Add(w);
      context.SaveChanges();
    }

    public List<Wiadomosc> getWiadomosc(int id)
    {
    /*
      var test = context.Rozmowa.Include(r => r.Wiadomosc).Single(k => k.TransakcjeID == id).Wiadomosc.ToList();
      test.ForEach(r =>
      {
        Console.WriteLine($"Wiadomosc {r.Tresc}");
      });
      */
     return context.Rozmowa.Include(r => r.Wiadomosc).SingleOrDefault(k => k.ID == id).Wiadomosc.ToList();
    }

    public List<Wiadomosc> getWiadomoscDlaXero(int id)
    {
      /*
        var test = context.Rozmowa.Include(r => r.Wiadomosc).Single(k => k.TransakcjeID == id).Wiadomosc.ToList();
        test.ForEach(r =>
        {
          Console.WriteLine($"Wiadomosc {r.Tresc}");
        });
        */
      return context.Rozmowa.Include(r => r.Wiadomosc).SingleOrDefault(k => k.UzytkownikID == id).Wiadomosc.ToList();
    }

    public List<PakietRozmowy> mojeRozmowyUzytkownik(int id)
    {
      
      
        List<PakietRozmowy> rozmowy = new List<PakietRozmowy>();
        context.Rozmowa.Where(r => r.UzytkownikID == id).ToList().ForEach(r =>
        {
          rozmowy.Add(new PakietRozmowy
          {
            Rozmowa = r,
            Uzytkownik = context.Uzytkownik.Single(u => u.ID == r.UzytkownikID),
            Xero = context.Xero.Single(x => x.ID == r.XeroID)
          });
        });
    
        return rozmowy;
      
     
  
    }
  }
}
