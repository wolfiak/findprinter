using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FindPrinter.Models;
using FindPrinter.Data;
using Projekto.Pakiet;
using Microsoft.AspNetCore.Http;
using System.IO;
using iTextSharp.text.pdf;
using Projekto.ToolBox;

namespace Projekto.Repository
{
  public class TransakcjeRepository : ITransakcje
  {
    private readonly ProjektContext context;

    public TransakcjeRepository(ProjektContext context)
    {
      this.context = context;
    }

    public int Add(Transakcje transakcje)
    {
      Uzytkownik uz=context.Uzytkownik.SingleOrDefault(u => u.ID == transakcje.UzytkownikIdo);
      transakcje.Nazwa = $"{uz.Imie} {uz.Nazwisko}";
      transakcje.Data = DateTime.Now;
      context.Add(transakcje);
      context.SaveChanges();
      return transakcje.ID;
    }

    public Transakcje Find(long key)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<Transakcje> GetAll()
    {
      throw new NotImplementedException();
    }
    public IEnumerable<Transakcje> FindAll(int id)
    {
   
      return context.Transakcje.Where(t => t.XeroIdo == id);
    }

    public void Remove(long key)
    {
      throw new NotImplementedException();
    }

    public void Update(UpdateTrans x)
    {
      Transakcje up = context.Transakcje.SingleOrDefault(k => k.ID == x.Id);
      up.PayPalId = x.PayPalId;
      up.StatusPlatnosci = x.StatusPlatnosci;
      context.Transakcje.Update(up);
      context.SaveChanges();
    }
    public void zmienStatus(PakietStatus pk)
    {
      context.Transakcje.Where(t =>  t.ID == pk.TransakcjaId && t.XeroIdo == pk.Id ).ToList().ForEach(t =>
       {
         t.Status = pk.Status;
       });
      context.SaveChanges();
    }

    public async Task<PakietPlik> ustalFormatICene(ICollection<IFormFile> files)
    {
      Console.WriteLine("Wbijam do dane");
      var filePath = "D:/FindPrinterTMP/";
      string patho = "";


      foreach (var file in files)
      {
        if (file.Length > 0)
        {
          string nazwa = file.FileName.Insert(0, Guid.NewGuid().ToString());
          patho = Path.Combine(filePath, nazwa);
          using (var stream = new FileStream(Path.Combine(filePath, nazwa), FileMode.Create))
          {
           await  file.CopyToAsync(stream);
          }
        }


      }
      Console.WriteLine($"Plik zapisany: " + patho);

      PdfReader reader = new PdfReader(patho);
      Console.WriteLine("Reader zlapany");
      int ilosc = reader.NumberOfPages;
      string format = reader.ustalFormat();
      PakietPlik pp = new PakietPlik(ilosc, format);
      if (System.IO.File.Exists(patho))
      {
        System.IO.File.Delete("patho");
      }
      Console.WriteLine($"Zwracam dane: {pp.Cena} {pp.Format} {pp.IloscStron} ");
    
      return pp;
    }

    public ICollection<Transakcje> czasPlynie(int id)
    {
      List<Transakcje> lista = FindAll(id).ToList<Transakcje>();
      for (int n = 0; n < lista.Count; n++)
      {

        Transakcje element = lista.ElementAt(n);
        Console.WriteLine("Pierwszy element: " + element.Piorytet);
        if (element.Piorytet.Equals("Na jutro"))
        {
          DateTime data = element.Data;
          data = data.AddDays(1);

          Console.WriteLine("Wypisuje datae: " + data.ToLongDateString());
          DataTools dt = new DataTools();
          dt.ticketWygasl += new EventHandler<Transakcje>(Wygasl);
          dt.naDzis += new EventHandler<Transakcje>(NaDzis);

          dt.porownaj(DataPorownanie.naPoziomieDni(data, DateTime.Now), element);
        }
        else if (element.Piorytet.Equals("Na dziś"))
        {
          Console.WriteLine("Zaszlo na dziś");
          DateTime data = element.Data;


          Console.WriteLine("Wypisuje date: " + data.ToLongDateString());
          DataTools dt = new DataTools();
          dt.ticketWygasl += new EventHandler<Transakcje>(Wygasl);
          dt.naDzis += new EventHandler<Transakcje>(NaDzis);
          dt.naZaraz += new EventHandler<Transakcje>(NaZaraz);
          dt.porownaj(DataPorownanie.naPoziomieDni(data, DateTime.Now), element);
        }
        else if (element.Piorytet.Equals("Na zaraz"))
        {
          DateTime data = element.Data;
          data = data.AddHours(1);

          Console.WriteLine("Wypisuje datae: " + data.ToLongDateString());
          DataTools dt = new DataTools();
          dt.ticketWygasl += new EventHandler<Transakcje>(Wygasl);
          dt.naDzis += new EventHandler<Transakcje>(NaZaraz);

          dt.porownaj(DataPorownanie.naPoziomieGodzin(data, DateTime.Now), element);
        }
        else
        {
          Console.WriteLine("NIE ZASZLO NIC");
        }
      }
      return lista;
    }
    public void Wygasl(object sender, Transakcje elemetnt)
    {
      Console.WriteLine("Zmieniam na opozniony");
      elemetnt.Piorytet = "Opóźniony";
      context.Transakcje.SingleOrDefault<Transakcje>(t => t.ID == elemetnt.ID).Piorytet = "Opóźniony";
      context.SaveChanges();
    }
    public void NaDzis(object sender, Transakcje elemetnt)
    {
      Console.WriteLine("Zmieniam na dzis");
      elemetnt.Piorytet = "Na dziś";
     context.Transakcje.SingleOrDefault<Transakcje>(t => t.ID == elemetnt.ID).Piorytet = "Na dziś";
     context.SaveChanges();
    }
    public void NaZaraz(object sender, Transakcje elemetnt)
    {
      Console.WriteLine("Zmieniam na dzis");
      elemetnt.Piorytet = "Na Zaraz";
     context.Transakcje.SingleOrDefault<Transakcje>(t => t.ID == elemetnt.ID).Piorytet = "Na Zaraz";
      context.SaveChanges();
    }
  }
}
