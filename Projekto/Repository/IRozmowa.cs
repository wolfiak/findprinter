using Projekto.Models;
using Projekto.Pakiet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Repository
{
    public interface IRozmowa
    {
    int Nowa(Rozmowa r);
      void NowaWiadomosc(Wiadomosc w);
      List<PakietRozmowy> mojeRozmowyXero(int id);
    List<PakietRozmowy> mojeRozmowyUzytkownik(int id);
    List<Wiadomosc> getWiadomoscDlaXero(int id);
    List<Wiadomosc> getWiadomosc(int id);
      
    }
}
