﻿using FindPrinter.Models;
using Projekto.Models;
using Projekto.Pakiet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FindPrinter.Repository
{
    public interface IXero
    {
        void Add(Xero xero);
        IEnumerable<Xero> GetAll();
        Xero Find(long key);
        void Remove(long key);
        void Update(Xero x);
        List<PakietWynik> bliskie(KoordynatyFront kor);
        void dodajOpinie(Opinia o);
        List<Opinia> getOpinie(int id);
        List<Xero> getXeroUzytkownika(int id);
  }
}
