﻿// <auto-generated />
using FindPrinter.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;

namespace Projekto.Migrations
{
    [DbContext(typeof(ProjektContext))]
    [Migration("20171213165320_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("FindPrinter.Models.Transakcje", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Cena");

                    b.Property<DateTime>("Data");

                    b.Property<int>("Ilosc");

                    b.Property<int>("IloscKopii");

                    b.Property<string>("Nazwa");

                    b.Property<string>("PayPalId");

                    b.Property<string>("Piorytet");

                    b.Property<string>("Sciezka");

                    b.Property<string>("Status");

                    b.Property<string>("StatusPlatnosci");

                    b.Property<int?>("UzytkownikID");

                    b.Property<int>("UzytkownikIdo");

                    b.Property<string>("Wiadomosc");

                    b.Property<int?>("XeroID");

                    b.Property<int>("XeroIdo");

                    b.HasKey("ID");

                    b.HasIndex("UzytkownikID");

                    b.HasIndex("XeroID");

                    b.ToTable("Transakcje");
                });

            modelBuilder.Entity("FindPrinter.Models.Uzytkownik", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<string>("Imie");

                    b.Property<string>("Klucz");

                    b.Property<string>("Nazwisko");

                    b.Property<string>("Password");

                    b.HasKey("ID");

                    b.ToTable("Uzytkownik");
                });

            modelBuilder.Entity("FindPrinter.Models.Xero", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Nazwa");

                    b.Property<string>("Szerokosc");

                    b.Property<int>("UzytkownikId");

                    b.Property<string>("Wysokosc");

                    b.Property<string>("avatar");

                    b.HasKey("ID");

                    b.HasIndex("UzytkownikId");

                    b.ToTable("Xero");
                });

            modelBuilder.Entity("Projekto.Models.Opinia", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Tresc");

                    b.Property<int?>("UzytkownikID");

                    b.Property<int?>("XeroID");

                    b.Property<bool>("thumb");

                    b.Property<int>("uzytkownikIdo");

                    b.Property<int>("xeroIdo");

                    b.HasKey("ID");

                    b.HasIndex("UzytkownikID");

                    b.HasIndex("XeroID");

                    b.ToTable("Opinia");
                });

            modelBuilder.Entity("Projekto.Models.Opis", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Tresc");

                    b.Property<int?>("XeroID");

                    b.HasKey("ID");

                    b.HasIndex("XeroID");

                    b.ToTable("Opis");
                });

            modelBuilder.Entity("Projekto.Models.Rozmowa", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("UzytkownikID");

                    b.Property<int>("XeroID");

                    b.HasKey("ID");

                    b.HasIndex("UzytkownikID");

                    b.ToTable("Rozmowa");
                });

            modelBuilder.Entity("Projekto.Models.Wiadomosc", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("RozmowaID");

                    b.Property<string>("Tresc");

                    b.Property<bool>("Xero");

                    b.HasKey("ID");

                    b.HasIndex("RozmowaID");

                    b.ToTable("Wiadomosc");
                });

            modelBuilder.Entity("FindPrinter.Models.Transakcje", b =>
                {
                    b.HasOne("FindPrinter.Models.Uzytkownik", "Uzytkownik")
                        .WithMany("Transakcje")
                        .HasForeignKey("UzytkownikID");

                    b.HasOne("FindPrinter.Models.Xero", "Xero")
                        .WithMany("Transakcje")
                        .HasForeignKey("XeroID");
                });

            modelBuilder.Entity("FindPrinter.Models.Xero", b =>
                {
                    b.HasOne("FindPrinter.Models.Uzytkownik")
                        .WithMany("Xero")
                        .HasForeignKey("UzytkownikId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Projekto.Models.Opinia", b =>
                {
                    b.HasOne("FindPrinter.Models.Uzytkownik", "Uzytkownik")
                        .WithMany()
                        .HasForeignKey("UzytkownikID");

                    b.HasOne("FindPrinter.Models.Xero", "Xero")
                        .WithMany("Opinia")
                        .HasForeignKey("XeroID");
                });

            modelBuilder.Entity("Projekto.Models.Opis", b =>
                {
                    b.HasOne("FindPrinter.Models.Xero", "Xero")
                        .WithMany("Opis")
                        .HasForeignKey("XeroID");
                });

            modelBuilder.Entity("Projekto.Models.Rozmowa", b =>
                {
                    b.HasOne("FindPrinter.Models.Uzytkownik")
                        .WithMany("Romzowa")
                        .HasForeignKey("UzytkownikID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Projekto.Models.Wiadomosc", b =>
                {
                    b.HasOne("Projekto.Models.Rozmowa")
                        .WithMany("Wiadomosc")
                        .HasForeignKey("RozmowaID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
