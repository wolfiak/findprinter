﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Projekto.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Uzytkownik",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Imie = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Klucz = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Nazwisko = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Uzytkownik", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Rozmowa",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UzytkownikID = table.Column<int>(type: "int", nullable: false),
                    XeroID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rozmowa", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Rozmowa_Uzytkownik_UzytkownikID",
                        column: x => x.UzytkownikID,
                        principalTable: "Uzytkownik",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Xero",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nazwa = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Szerokosc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UzytkownikId = table.Column<int>(type: "int", nullable: false),
                    Wysokosc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    avatar = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Xero", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Xero_Uzytkownik_UzytkownikId",
                        column: x => x.UzytkownikId,
                        principalTable: "Uzytkownik",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Wiadomosc",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RozmowaID = table.Column<int>(type: "int", nullable: false),
                    Tresc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Xero = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Wiadomosc", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Wiadomosc_Rozmowa_RozmowaID",
                        column: x => x.RozmowaID,
                        principalTable: "Rozmowa",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Opinia",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Tresc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UzytkownikID = table.Column<int>(type: "int", nullable: true),
                    XeroID = table.Column<int>(type: "int", nullable: true),
                    thumb = table.Column<bool>(type: "bit", nullable: false),
                    uzytkownikIdo = table.Column<int>(type: "int", nullable: false),
                    xeroIdo = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Opinia", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Opinia_Uzytkownik_UzytkownikID",
                        column: x => x.UzytkownikID,
                        principalTable: "Uzytkownik",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Opinia_Xero_XeroID",
                        column: x => x.XeroID,
                        principalTable: "Xero",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Opis",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Tresc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    XeroID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Opis", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Opis_Xero_XeroID",
                        column: x => x.XeroID,
                        principalTable: "Xero",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Transakcje",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Cena = table.Column<int>(type: "int", nullable: false),
                    Data = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Ilosc = table.Column<int>(type: "int", nullable: false),
                    IloscKopii = table.Column<int>(type: "int", nullable: false),
                    Nazwa = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PayPalId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Piorytet = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Sciezka = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StatusPlatnosci = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UzytkownikID = table.Column<int>(type: "int", nullable: true),
                    UzytkownikIdo = table.Column<int>(type: "int", nullable: false),
                    Wiadomosc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    XeroID = table.Column<int>(type: "int", nullable: true),
                    XeroIdo = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transakcje", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Transakcje_Uzytkownik_UzytkownikID",
                        column: x => x.UzytkownikID,
                        principalTable: "Uzytkownik",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Transakcje_Xero_XeroID",
                        column: x => x.XeroID,
                        principalTable: "Xero",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Opinia_UzytkownikID",
                table: "Opinia",
                column: "UzytkownikID");

            migrationBuilder.CreateIndex(
                name: "IX_Opinia_XeroID",
                table: "Opinia",
                column: "XeroID");

            migrationBuilder.CreateIndex(
                name: "IX_Opis_XeroID",
                table: "Opis",
                column: "XeroID");

            migrationBuilder.CreateIndex(
                name: "IX_Rozmowa_UzytkownikID",
                table: "Rozmowa",
                column: "UzytkownikID");

            migrationBuilder.CreateIndex(
                name: "IX_Transakcje_UzytkownikID",
                table: "Transakcje",
                column: "UzytkownikID");

            migrationBuilder.CreateIndex(
                name: "IX_Transakcje_XeroID",
                table: "Transakcje",
                column: "XeroID");

            migrationBuilder.CreateIndex(
                name: "IX_Wiadomosc_RozmowaID",
                table: "Wiadomosc",
                column: "RozmowaID");

            migrationBuilder.CreateIndex(
                name: "IX_Xero_UzytkownikId",
                table: "Xero",
                column: "UzytkownikId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Opinia");

            migrationBuilder.DropTable(
                name: "Opis");

            migrationBuilder.DropTable(
                name: "Transakcje");

            migrationBuilder.DropTable(
                name: "Wiadomosc");

            migrationBuilder.DropTable(
                name: "Xero");

            migrationBuilder.DropTable(
                name: "Rozmowa");

            migrationBuilder.DropTable(
                name: "Uzytkownik");
        }
    }
}
