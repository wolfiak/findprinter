﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Pakiet
{
    public class PakietStatus
    {
    public int Id { get; set; }
    public string Status { get; set; }
    public int TransakcjaId { get; set; }

  }
}
