using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Pakiet
{
    public class UpdateTrans
    {
    public int Id { get; set; }
    public string PayPalId { get; set; }
    public string StatusPlatnosci { get; set; }
  }
}
