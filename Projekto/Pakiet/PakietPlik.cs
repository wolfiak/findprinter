using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Pakiet
{
    public class PakietPlik
    {

    public PakietPlik( int IloscStron, string Format)
    {
      
      this.IloscStron = IloscStron;
      this.Format = Format;
      if(Format == "A4")
      {
        Cena = 1.10 * IloscStron;
      }
    }

    public double Cena { get; set; }
    public int IloscStron { get; set; }
    public string Format { get; set; }
  }
}
