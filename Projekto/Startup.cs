using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.IO;
using FindPrinter.Data;
using FindPrinter.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Projekto.Repository;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using System;
using Projekto.Auth;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Projekto.Data;

namespace Projekto
{
  public class Startup
  {
    public IConfigurationRoot Configuration { get; }
    public Startup(IHostingEnvironment env)
    {
      var builder = new ConfigurationBuilder()
                 .SetBasePath(env.ContentRootPath)
                 .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                 .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                 .AddEnvironmentVariables();
      Configuration = builder.Build();




    }
    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
 
      services.AddDbContext<ProjektContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
      services.AddApplicationInsightsTelemetry(Configuration);
      services.AddMvc();

      /*services.AddAuthorization(auth =>
      {
        auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
            .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme‌​)
            .RequireAuthenticatedUser().Build());
      });
      

      services.AddAuthentication(o =>
      {
        o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
      });
      */
      var tokenProvider = new RSAKeyHelper("issuer","audience","dupa1");
      services.AddSingleton<ITokenProvider>(tokenProvider);

      services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(options =>
        {
          options.RequireHttpsMetadata = false;
          options.TokenValidationParameters = tokenProvider.GetValidationParameters();
        });

      services.AddAuthorization(auth =>
      {
        auth.DefaultPolicy = new AuthorizationPolicyBuilder()
          .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
          .RequireAuthenticatedUser()
          .Build();
      });
      services.AddScoped<IXero, XeroRepository>();
      services.AddScoped<IUzytkownik, UzytkownikRepository>();
      services.AddScoped<ITransakcje, TransakcjeRepository>();
      services.AddScoped<IRozmowa,RozmowaRepository>();


      var corsBuilder = new CorsPolicyBuilder();
      corsBuilder.AllowAnyHeader();
      corsBuilder.AllowAnyMethod();
      corsBuilder.AllowAnyOrigin(); // For anyone access.
                                    //corsBuilder.WithOrigins("http://localhost:56573"); // for a specific url. Don't add a forward slash on the end!
      corsBuilder.AllowCredentials();

      services.AddCors(options =>
      {
        options.AddPolicy("SiteCorsPolicy", corsBuilder.Build());
      });


    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
    {
      app.UseAuthentication();

      app.UseExceptionHandler(appBuilder =>
      {
        appBuilder.Use(async (context, next) =>
        {
          var error = context.Features[typeof(IExceptionHandlerFeature)] as IExceptionHandlerFeature;

          //when authorization has failed, should retrun a json message to client
          if (error != null && error.Error is SecurityTokenExpiredException)
          {
            context.Response.StatusCode = 401;
            context.Response.ContentType = "application/json";

            await context.Response.WriteAsync(JsonConvert.SerializeObject(
                new { authenticated = false, tokenExpired = true }
            ));
          }
          //when orther error, retrun a error message json to client
          else if (error != null && error.Error != null)
          {
            context.Response.StatusCode = 500;
            context.Response.ContentType = "application/json";
            await context.Response.WriteAsync(JsonConvert.SerializeObject(
                new { success = false, error = error.Error.Message }
            ));
          }
          //when no error, do next.
          else await next();
        });
      });


     /* 
      var options = new JwtBearerOptions();
      options.TokenValidationParameters.IssuerSigningKey = TokenAuthOption.Key;
      options.TokenValidationParameters.ValidAudience = TokenAuthOption.Audience;
      options.TokenValidationParameters.ValidIssuer = TokenAuthOption.Issuer;
      options.TokenValidationParameters.ValidateIssuerSigningKey = true;
      options.TokenValidationParameters.ValidateLifetime = true;
      options.TokenValidationParameters.ClockSkew = TimeSpan.FromMinutes(0);
      app.UseJwtBearerAuthentication(options);
      app.UseJwtBearerAuthentication(options);
      app.Use(async (context, next) =>
            {
              await next();
              if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value) &&
              !context.Request.Path.Value.StartsWith("/api/"))
              {
                context.Request.Path = "/index.html";
                await next();
              }
            });
*/
      app.UseMvcWithDefaultRoute();


      app.UseDefaultFiles();
      app.UseStaticFiles();
    }
  }
}
