using FindPrinter.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Projekto.Auth
{
    public class Token
    {
    public string acess_token { get; set; }
    public string token_type { get; set; } = "bearer";
    public int expires_in { get; set; }
    public string refresh_token { get; set; }
    public int id { get; set; }
  }
}
