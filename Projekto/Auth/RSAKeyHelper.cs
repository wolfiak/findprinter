using Microsoft.IdentityModel.Tokens;
using Projekto.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using FindPrinter.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;

namespace Projekto.Data
{
    public class RSAKeyHelper :ITokenProvider
    {
    private RsaSecurityKey key;
    private string algorithm;
    private string issuer;
    private string audience;

    public RSAKeyHelper(string issuer, string audience, string keyName) 
    {
      var parameters = new CspParameters { KeyContainerName = keyName };
      var provider = new RSACryptoServiceProvider(2048, parameters);

      key = new RsaSecurityKey(provider);

      algorithm = SecurityAlgorithms.RsaSha256Signature;
      this.issuer = issuer;
      this.audience = audience;

    }

    public string CreateToken(Uzytkownik uz, DateTime wygasa)
    {
      JwtSecurityTokenHandler th = new JwtSecurityTokenHandler();

      ClaimsIdentity i = new ClaimsIdentity(new GenericIdentity(uz.Email, "jwt"));

      SecurityToken token = th.CreateJwtSecurityToken(new SecurityTokenDescriptor
      {
        Audience=audience,
        Issuer=issuer,
        SigningCredentials=new SigningCredentials(key,algorithm),
        Expires=wygasa.ToUniversalTime(),
        Subject=i
      });

      return th.WriteToken(token);
    }

    public TokenValidationParameters GetValidationParameters()
    {
      return new TokenValidationParameters
      {
        IssuerSigningKey = key,
        ValidAudience = audience,
        ValidIssuer = issuer,
        ValidateLifetime = true,
        ClockSkew = TimeSpan.FromSeconds(0)
      };
    }
  }
}
