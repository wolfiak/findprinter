using FindPrinter.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Auth
{
    public interface ITokenProvider
    {
    string CreateToken(Uzytkownik uz, DateTime wygasa);

    TokenValidationParameters GetValidationParameters();
    }
}
