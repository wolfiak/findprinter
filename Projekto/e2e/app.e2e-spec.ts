import { AngularoPage } from './app.po';

describe('angularo App', () => {
  let page: AngularoPage;

  beforeEach(() => {
    page = new AngularoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
