using FindPrinter.Models;
using Microsoft.EntityFrameworkCore;
using Projekto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FindPrinter.Data
{
    public class ProjektContext : DbContext
    {
        public ProjektContext(DbContextOptions<ProjektContext> options): base(options)
        {}

        public virtual DbSet<Xero> Xero { get; set; }
        public virtual DbSet<Transakcje> Transakcje { get; set; }
        public virtual DbSet<Uzytkownik> Uzytkownik { get; set; }
    public virtual DbSet<Opinia> Opinia { get; set; }
    public virtual DbSet<Opis> Opis { get; set; }
    public virtual DbSet<Wiadomosc> Wiadomosc { get; set; }
    public virtual DbSet<Rozmowa> Rozmowa { get; set; }
 
  }
}
