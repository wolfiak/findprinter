using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Projekto.Models;
using Projekto.Pakiet;
using Projekto.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Controllers
{
  [EnableCors("SiteCorsPolicy")]
  [Route("api/[controller]")]
    public class RozmowaController :Controller
    {

    private readonly IRozmowa repo;

    public RozmowaController(IRozmowa repo)
    {
      this.repo = repo;
    }

    [HttpPost("nowaRozmowa")]
      public IActionResult NowaRozmowa([FromBody] Rozmowa r)
    {
      if(r== null)
      {
        return BadRequest();
      }
      
      int wynik=repo.Nowa(r);
      return Ok(new { ID=wynik});
    }
    [HttpGet("mojeRozmowyXero")]
    public IActionResult mojeRomowyXero(int xeroID)
    {
      Console.WriteLine($"Zapytanie o xero: {xeroID}");
      List<PakietRozmowy> rozm = repo.mojeRozmowyXero(xeroID);
      return Ok(JsonConvert.SerializeObject(rozm,Formatting.Indented,
        new JsonSerializerSettings
        {
          ReferenceLoopHandling=ReferenceLoopHandling.Ignore
        }
        ));
    }
    [HttpGet("mojeRozmowyUzytkownik")]
    public IActionResult mojeRomowyUzytkownik(int uzytkownikID)
    {
      Console.WriteLine($"Zapytanie o xero: {uzytkownikID}");
      List<PakietRozmowy> rozm = repo.mojeRozmowyUzytkownik(uzytkownikID);
      return Ok(JsonConvert.SerializeObject(rozm, Formatting.Indented,
        new JsonSerializerSettings
        {
          ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        }
        ));
    }
    [HttpPost("nowaWiadomosc")]
    public IActionResult nowaWiadomosc([FromBody]Wiadomosc w)
    {
      if(w == null)
      {
        return BadRequest();
      }
      Console.WriteLine($"Wiadomosc : {w.Tresc}");
      repo.NowaWiadomosc(w);

      return Ok();

    }
    [HttpGet("getWiadomosc")]
    public IEnumerable<Wiadomosc> getWiadomosc(int id)
    {
      Console.WriteLine("Zwracam wiadomosci dla transakji: "+id);
      List<Wiadomosc> lista = repo.getWiadomosc(id);
      Console.WriteLine($"lista rozmiar: {lista.Count}");
      return lista;
    }

    [HttpGet("getWiadomoscDlaXero")]
    public IEnumerable<Wiadomosc> getWiadomoscDlaXero(int id)
    {
      Console.WriteLine("Zwracam wiadomosci dla transakji: " + id);
      List<Wiadomosc> lista = repo.getWiadomoscDlaXero(id);
      Console.WriteLine($"lista rozmiar: {lista.Count}");
      return lista;
    }

  }
}
