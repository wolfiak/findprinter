using FindPrinter.Models;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Projekto.Models;
using Projekto.Pakiet;
using Projekto.Repository;
using Projekto.ToolBox;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Controllers
{
  [EnableCors("SiteCorsPolicy")]
  [Route("api/[controller]")]
  public class TransakcjeController : Controller
  {
    private readonly ITransakcje repo;


    public TransakcjeController(ITransakcje repo)
    {
      this.repo = repo;
    }

    [HttpPost]
    public IActionResult Post([FromBody] Transakcje tr)
    {
      Console.WriteLine("             Jestem w POST");
      if (tr == null)
      {
        Console.WriteLine("Zaszedl null");
        return BadRequest();
      }
      int id=repo.Add(tr);
      return Ok(new { id=id });
    }
    [HttpPut]
    public IActionResult Put([FromBody] UpdateTrans transakcje)
    {

      repo.Update(transakcje);

      return Ok();
    }
    [HttpPost("plik")]
    public async Task<IActionResult> Plik(ICollection<IFormFile> files)
    {
      var filePath = "D:/FindPrinterTMP/";
      string patho = "";


      foreach (var file in files)
      {
        if(file.Length > 0)
        {
          string nazwa = file.FileName.Insert(0, Guid.NewGuid().ToString());
           patho = Path.Combine(filePath, nazwa);
          using (var stream = new FileStream(Path.Combine(filePath, nazwa), FileMode.Create))
          {
            await file.CopyToAsync(stream);
          }
        }
      }
      return Ok(new { Path = patho });
    }
    [HttpPost("dane")]
    public async Task<IActionResult> Dane(ICollection<IFormFile> files)
    {
      PakietPlik pp = await repo.ustalFormatICene(files);
      
      return Ok(pp);
    }
    [HttpPost("wezTransakcje")]
    public IActionResult wezTransakcje([FromBody] int id)
    {

      ICollection<Transakcje> lista=repo.czasPlynie(id);


      return Ok(new { Transakcje=lista});
    }
    
    [HttpPost("zmienStatus")]
    public IActionResult zmienStatus([FromBody] PakietStatus pk)
    {
      repo.zmienStatus(pk);
      return Ok();
    }

    [HttpGet("getFile")]
    public  IActionResult download(string path)
    {
      var stream = new FileStream(path, FileMode.Open);
      return  File(stream, "application/pdf");
    }
    [HttpGet("data")]
    public IActionResult data()
    {
      return Ok(new { data = DateTime.Now });
    }

  }
}
