using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FindPrinter.Data;
using FindPrinter.Models;
using FindPrinter.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Newtonsoft.Json;
using Projekto.Models;
using Projekto.Pakiet;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FindPrinter.Controllers
{
 
 
  [EnableCors("SiteCorsPolicy")]
  [Route("api/[controller]")]
    public class XeroController : Controller
    {
        private readonly IXero repo;
        public XeroController(IXero repo)
        {
          this.repo = repo;
        }
        // GET: api/values
        [HttpGet]
        public IEnumerable<Xero> Get()
        {
          return repo.GetAll();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Xero item = repo.Find(id);
            if(item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
       
    public IActionResult Post([FromBody]Xero value)
        {
            if(value== null)
            {
                return BadRequest();
            }
            repo.Add(value);

            return Ok(JsonConvert.SerializeObject(new { wszystko= "ok"}));
        }
        [HttpPost("bliskie")]
        public IActionResult bliskie([FromBody]KoordynatyFront kor)
        {
          if(kor == null)
          {
            return BadRequest();
          }
          
           List<PakietWynik> koro=repo.bliskie(kor);
          
           return Ok(JsonConvert.SerializeObject(koro,Formatting.Indented,
             new JsonSerializerSettings
             {
               ReferenceLoopHandling=ReferenceLoopHandling.Ignore
             }));
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        
        [HttpPost("opinia")]
        public IActionResult Opinia([FromBody] Opinia o)
        {
          if (o == null)
          {
            return BadRequest();
          }
           repo.dodajOpinie(o);

          return Ok(JsonConvert.SerializeObject(new { wszystko = "ok" }));
        }
        
        [HttpGet("getOpinie")]
        public IEnumerable<Opinia> getOpinie(int id)
        {
            return repo.getOpinie(id);
        }
        [HttpPost("xerouz")]
        public IActionResult getXeroUzytkownika([FromBody] int id)
        {
       
         List<Xero> wynik= repo.getXeroUzytkownika(id);
        return Ok(JsonConvert.SerializeObject(wynik, Formatting.Indented,
           new JsonSerializerSettings
           {
             ReferenceLoopHandling = ReferenceLoopHandling.Ignore
           }));

    }
    }
}
