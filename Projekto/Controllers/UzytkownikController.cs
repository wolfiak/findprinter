using FindPrinter.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Projekto.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Controllers
{
  [EnableCors("SiteCorsPolicy")]
  [Route("api/[controller]")]
  public class UzytkownikController : Controller
    {
       private readonly IUzytkownik repo;


      public UzytkownikController(IUzytkownik repo)
    {
      this.repo = repo;
    }

    [HttpGet]
    public IEnumerable<Uzytkownik> Get()
    {
      return repo.GetAll();
    }

    [HttpGet("getOne")]
    public IActionResult Get(int id)
    {
      Uzytkownik uz = repo.Find(id);
      if(uz == null)
      {
        return NotFound();
      }
      return new ObjectResult(uz);
    }

    [HttpPost("login")]
    public IActionResult Logowanie([FromBody] Uzytkownik uz)
    {
      return Ok(repo.Login(uz));
    }

    [HttpPost]
    public IActionResult Post([FromBody]Uzytkownik uz)
    {
      if(uz == null)
      {
        return BadRequest();
      }
      repo.Add(uz);
      return Ok();
    }
    [HttpGet("zalogowany")]
    [Authorize]
    public IActionResult Zalogowany()
    {
      return Ok();
    }


    }
}
