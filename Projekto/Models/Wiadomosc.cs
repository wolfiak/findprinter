using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Models
{
    public class Wiadomosc
    {
    public int ID { get; set; }

    public bool Xero { get; set; }

    public string Tresc { get; set; }

    public virtual int RozmowaID { get; set; }
  }
}
