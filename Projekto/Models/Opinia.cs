﻿using FindPrinter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Models
{
  public class Opinia
  {


    public int ID { get; set; }
    public string Tresc { get; set; }
    public bool thumb { get; set; }
    public int uzytkownikIdo { get; set; }

    public int xeroIdo { get; set; }

    public virtual Uzytkownik Uzytkownik { get; set; }
    public virtual Xero Xero { get; set; }
  }
}
