using Projekto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FindPrinter.Models
{
  public class Transakcje
  {
    public int ID { get; set; }
    public string Sciezka { get; set; }
    public int IloscKopii { get; set; }
    public string Piorytet { get; set; }
    public string Wiadomosc { get; set; }
    public string StatusPlatnosci { get; set; }
    public string Status { get; set; }
    public int Cena { get; set; }
    public int Ilosc { get; set; }
    public string PayPalId { get; set; }
    public string  Nazwa { get; set; }
    public DateTime Data { get; set; }

    public virtual Uzytkownik Uzytkownik { get; set; }
    public int UzytkownikIdo { get; set; }
    public virtual Xero Xero { get; set; }
//    public virtual Rozmowa Romzowa { get; set; }
    public int XeroIdo { get; set; }
  }
}
