using Projekto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FindPrinter.Models
{
    public class Uzytkownik
    {
        public int ID { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Klucz { get; set; }

    public virtual ICollection<Xero> Xero { get; set; }
    public virtual ICollection<Rozmowa> Romzowa { get; set; }
    public virtual ICollection<Transakcje> Transakcje { get; set; }
    }
}
