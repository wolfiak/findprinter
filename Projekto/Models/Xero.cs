using Projekto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FindPrinter.Models
{
  public class Xero
  {
    public int ID { get; set; }
    public string Nazwa { get; set; }
    public string Szerokosc { get; set; }
    public string Wysokosc { get; set; }
    public string avatar { get; set; }
   // public int UzytkownikId { get; set; }
    public virtual int UzytkownikId { get; set; }

  //  public virtual ICollection<Rozmowa> Romzowa { get; set; }
    public virtual ICollection<Opis> Opis { get; set; }
    public virtual ICollection<Opinia> Opinia { get; set; }
    public virtual ICollection<Transakcje> Transakcje { get; set; }
  }
}
