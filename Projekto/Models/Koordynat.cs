﻿using FindPrinter.Models;
using GeoCoordinatePortable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Models
{
    public class Koordynat
    {

    public Koordynat()
    {

    }
    public Koordynat(int xeroId, GeoCoordinate geo)
    {
      this.xeroId = xeroId;
      this.geo = geo;
    }
    public string Szerokosc { get; set; }
    public string Wysokosc { get; set; }
    public int xeroId { get; set; }
    public GeoCoordinate geo { get; set; }
  }
}
