﻿using FindPrinter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.Models
{
    public class Opis
    {
    public Opis()
    {

    }
    public Opis(Xero xero, string Tresc)
    {
      this.Xero = xero;
      this.Tresc = Tresc;
    }
    public int ID { get; set; }
    public string Tresc { get; set; }

    public virtual Xero Xero { get; set; }
  }
}
