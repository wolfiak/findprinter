import { TestBed, inject } from '@angular/core/testing';

import { OpiniaService } from './opinia.service';

describe('OpiniaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OpiniaService]
    });
  });

  it('should be created', inject([OpiniaService], (service: OpiniaService) => {
    expect(service).toBeTruthy();
  }));
});
