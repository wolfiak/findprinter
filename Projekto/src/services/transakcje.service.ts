import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from './../environments/environment';
import { ITransakcja } from './../model/ITransakcja';

@Injectable()
export class TransakcjeService {
    private URL: string = environment.URL + 'api/transakcje/plik';
    private URL2: string = environment.URL + 'api/transakcje';
    private URL3: string = environment.URL + 'api/transakcje/dane';
    constructor(private http: Http) { }

    wyslijPlik(plik) {
        let fileList: FileList = plik.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let formData: FormData = new FormData();
            formData.append('files', file);
            
            let headers = new Headers();
            let token = JSON.parse(localStorage.getItem('auth_token'));
            headers.append('enctype', "multipart/form-data");
            headers.append('Authorization', token.token);
            let options = new RequestOptions({ headers: headers });
            return this.http.post(this.URL, formData ,options)
                .map(res => {
                    let reso = res.json();
                    return reso;
                });

        }

    }
    wyslijDane(plik) {
      let fileList: FileList = plik.target.files;
      if (fileList.length > 0) {
        let file: File = fileList[0];
        let formData: FormData = new FormData();
        formData.append('files', file);

        let headers = new Headers();
        let token = JSON.parse(localStorage.getItem('auth_token'));
        headers.append('enctype', "multipart/form-data");
        headers.append('Authorization', token.token);
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.URL3, formData, options)
          .map(res => {
            let reso = res.json();
            return reso;
          });

      }

    }
    stworzTransakcje(path: ITransakcja) {
        let headers = new Headers();
        let token = JSON.parse(localStorage.getItem('auth_token'));
        headers.append('Content-Type', "application/json");
        headers.append('Authorization', token.token);

        return this.http.post(this.URL2, JSON.stringify(path), { headers })
            .map(res => {
                console.log(res);

                return res.json();
            });
    }

}
