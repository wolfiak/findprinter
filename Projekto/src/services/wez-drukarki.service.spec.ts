import { TestBed, inject } from '@angular/core/testing';

import { WezDrukarkiService } from './wez-drukarki.service';

describe('WezDrukarkiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WezDrukarkiService]
    });
  });

  it('should be created', inject([WezDrukarkiService], (service: WezDrukarkiService) => {
    expect(service).toBeTruthy();
  }));
});
