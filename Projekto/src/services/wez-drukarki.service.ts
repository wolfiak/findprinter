import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { environment } from './../environments/environment';
import { IKoordynaty } from './../model/IKoordynaty';

@Injectable()
export class WezDrukarkiService {
    private URL: string = environment.URL + 'api/xero/bliskie';
    private tablica: any[] = [];
    private lokalizacja: any;
    
  constructor(private http: Http, private router: Router) { }
  
  wez(kor: IKoordynaty, lok: string) {
      this.lokalizacja = {
          lokalizacja: lok,
          koordynaty: kor
      };
      let headers = new Headers();
      let token = JSON.parse(localStorage.getItem('auth_token'));
      headers.append('Content-Type', "application/json");
      headers.append('Authorization', token.token);
      this.http.post(this.URL, JSON.stringify(kor), { headers }).subscribe(
          res => {
              console.log("WYNIKI GETA: ");
              let reso = res.json();
              this.tablica = reso;
              console.log(reso);
          },
          err => console.log(err),
          () => {
            this.router.navigateByUrl('/switch/(aux:wynik)');
          }
      );
  }
  public getTab() {
      return new Promise((res) => {
          setInterval(() => {
              if (this.tablica.length>0) {
                  res(this.tablica);
              }
          },100)
      });
  }
  getLokalizacja() {
      return this.lokalizacja;
  }



}
