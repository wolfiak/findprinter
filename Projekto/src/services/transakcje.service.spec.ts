import { TestBed, inject } from '@angular/core/testing';

import { TransakcjeService } from './transakcje.service';

describe('TransakcjeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TransakcjeService]
    });
  });

  it('should be created', inject([TransakcjeService], (service: TransakcjeService) => {
    expect(service).toBeTruthy();
  }));
});
