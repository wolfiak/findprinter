import { TestBed, inject } from '@angular/core/testing';

import { CzatProviderService } from './czat-provider.service';

describe('CzatProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CzatProviderService]
    });
  });

  it('should be created', inject([CzatProviderService], (service: CzatProviderService) => {
    expect(service).toBeTruthy();
  }));
});
