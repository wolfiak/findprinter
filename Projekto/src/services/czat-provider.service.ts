import { Injectable } from '@angular/core';

@Injectable()
export class CzatProviderService {
  private ID: number;
  private mozna: boolean;
  constructor() { }
  
  setID(id) {
    console.log("Czat Provider Service: " + id);
    this.ID = id;
    this.mozna = true;
  }
  getMozna() {
    return this.mozna;
  }
  getID() {
    this.mozna = false;
    return this.ID;
  }
}
