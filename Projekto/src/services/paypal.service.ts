import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from './../environments/environment';

@Injectable()
export class PaypalService {
  private URL: string = environment.urlApi + 'pay';

  constructor(private http: Http) { }

  wykonajPlatnosc(obiekt) {
    console.log("OBIEKT:");
    console.log(JSON.stringify(obiekt));
    let headers = new Headers();
    let token = JSON.parse(localStorage.getItem('auth_token'));
    headers.append('Authorization', token.token);
    headers.append('Content-Type', 'application/json');
     return this.http.post(this.URL, obiekt, { headers })
      .map(res => {
       // let reso = res.json();
        console.log('respond:');
        console.log("PAYPAL: ");
        console.log(res);
        window.location.href = res.json().url;
        return true;
      });
  }


}
