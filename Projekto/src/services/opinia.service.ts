﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { IOpinia } from './../model/IOpinia';
import { environment } from './../environments/environment';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class OpiniaService {
    private URL: string = environment.URL + 'api/xero/opinia';
    private URL2: string = environment.URL + 'api/xero/getOpinie';
    private URL3: string = environment.URL + 'api/uzytkownik/getOne';
    private sub:Subject<any> = new Subject<any>();
  constructor(private http: Http) { }

  wyslij(opinia: IOpinia) {
      let headers = new Headers();
      let token = JSON.parse(localStorage.getItem('auth_token'));
      headers.append('Content-Type', "application/json");
      headers.append('Authorization', token.token);

      return this.http.post(this.URL, JSON.stringify(opinia), { headers })
          .map(res => {
              console.log(res);
             
              return true;
          });

  }
  getOpinie(id: string) {
      let headers = new Headers();
      headers.append('Content-Type', "application/json");

      return this.http.get(this.URL2+`?id=${id}`, { headers })
          .map(res => {

              return res.json();
          })
  }
  getUz(tab) {
      let headers = new Headers();
      headers.append('Content-Type', "application/json");
    
      for (let n = 0; n < tab.length; n++) {
           this.http.get(this.URL3 + `?id=${tab[n].xeroIdo}`, { headers })
              .subscribe(res => {
                  let reso = res.json();
                  console.log(reso);
                  tab[n].imie = reso.imie;
                  tab[n].nazwisko = reso.nazwisko;
                  this.sub.next(tab);
              });
      }
      return this.sub;
     

  }
}
