﻿import { Injectable } from '@angular/core';
import { Uzytkownik } from './../model/IUzytkownik';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from './../environments/environment';
import { Router } from '@angular/router';
@Injectable()
export class RejestracjaService {
    private URL: string = environment.URL + 'api/uzytkownik';
    constructor(private http: Http, private router: Router) { }

  rejestruj(uz: Uzytkownik) {
      let body = JSON.stringify(uz);
      console.log("PRZYKLADOWE BODY");
      
      console.log(body);
      console.log("czysty uczytkownik:");
      console.log(uz);
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });

      this.http.post(this.URL, body, options).subscribe(
          data => console.log(data),
          error => console.log(error),
          () => {
              this.router.navigate(['logo']);
          }
      );
  }

}
