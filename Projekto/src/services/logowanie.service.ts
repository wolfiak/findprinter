import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from './../environments/environment';
import { Uzytkownik } from './../model/IUzytkownik';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';


@Injectable()
export class LogowanieService {
    private URL: string = environment.URL + 'api/uzytkownik/login';
    private URL2: string = environment.URL + 'api/uzytkownik/zalogowany';
    private URL3: string = environment.URL + 'api/uzytkownik/getOne';
    private loggedIn: boolean = false;
    private userID: number;
    constructor(private http: Http) { }
    

  loguj(passy: Uzytkownik) {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      return this.http.post(this.URL, JSON.stringify(passy), { headers })
        .map(res => {
          console.log("Zwrot z logowania: ");
          
          let reso = res.json();
          console.log(reso);
              let tok = {
                  token: 'Bearer ' + reso.acess_token,
                  id: reso.id
              };
              this.userID = reso.id;
              localStorage.setItem('auth_token', JSON.stringify(tok));
              this.loggedIn = true;
              return true;
          });
        
          
          
    
   /*         let reso = res.json();
      localStorage.setItem('auth_token', reso.accessToken);
      this.loggedIn = true;
      return true;
          */
   
  }
  isLoggedIn() {
      console.log("Sprawdzam tu");
      let headers = new Headers();
      let token = JSON.parse(localStorage.getItem('auth_token'));
      headers.append('Content-Type', "application/json");
      headers.append('Authorization', token.token);
      return this.http.get(this.URL2, { headers });
      
  }
  logout() {
      localStorage.removeItem('auth_token');
      this.loggedIn = false;
  }
  getUz(id) {
    let headers = new Headers();
    headers.append('Content-Type', "application/json");

    return this.http.get(this.URL3 + `?id=${id}`, { headers })
      .map(res => {
        console.log("Uzytkownik:");
        console.log(res)

        return res.json();

      });
  }
  getUserId() {
    if (this.userID) {
      return this.userID;
    } else {
      return JSON.parse(localStorage.getItem('auth_token')).id;
    }
   
  }
}
