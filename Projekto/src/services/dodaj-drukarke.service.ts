import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from './../environments/environment';
import { IXero } from './../model/IXero';
@Injectable()
export class DodajDrukarkeService {
    private URL: string = environment.URL + 'api/xero';
  constructor(private http: Http) { }

  dodaj(info) {
      let headers = new Headers();
      let token = JSON.parse(localStorage.getItem('auth_token'));
      headers.append('Content-Type', "application/json");
      headers.append('Authorization', token.token);
      return this.http.post(this.URL, JSON.stringify(info), { headers })
          .map(res => {
              let reso = res.json();
              console.log(reso);
              return true;
          });

  }
}
