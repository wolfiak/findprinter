import { TestBed, inject } from '@angular/core/testing';

import { DodajDrukarkeService } from './dodaj-drukarke.service';

describe('DodajDrukarkeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DodajDrukarkeService]
    });
  });

  it('should be created', inject([DodajDrukarkeService], (service: DodajDrukarkeService) => {
    expect(service).toBeTruthy();
  }));
});
