﻿export interface IXero {
    nazwa: string,
    szerokosc: string,
    wysokosc: string,
    opis: any[],
    uzytkownikId: string,
    avatar: string

}