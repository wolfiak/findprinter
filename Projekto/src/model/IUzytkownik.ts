﻿export interface Uzytkownik {
    imie: string,
    nazwisko: string,
    email: string,
    password: string
}