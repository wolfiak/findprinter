﻿export interface ITransakcja {
    Sciezka: string,
    UzytkownikIdo: string,
    XeroIdo: string,
    Format: string,
    IloscKopii: string,
    Piorytet: string,
    Wiadomosc: string

}