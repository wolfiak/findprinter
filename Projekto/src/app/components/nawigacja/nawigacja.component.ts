import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nawigacja',
  templateUrl: './nawigacja.component.html',
  styleUrls: ['./nawigacja.component.css']
})
export class NawigacjaComponent implements OnInit {
  poka: boolean = false;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  profilPoka() {
    let el = document.getElementById("trzy");
    let i = document.getElementById("ikona");
    if (this.poka) {
      this.poka = false;
      el.className = "";
      i.className = "fa fa-chevron-down";
    } else {
      this.poka = true;
      el.className += " szerokosc";
      i.className += " poPrawej";
    }
  }

  prowadz(gdzie) {
    if (gdzie == 1) {
      this.router.navigateByUrl('switch/(aux:wiadomosc)');
    }
  }

}
