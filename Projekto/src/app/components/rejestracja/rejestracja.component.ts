﻿import { Component, OnInit } from '@angular/core';
import { RejestracjaService } from './../../../services/rejestracja.service';
import { Uzytkownik } from './../../../model/IUzytkownik';

@Component({
  selector: 'app-rejestracja',
  templateUrl: './rejestracja.component.html',
  styleUrls: ['./rejestracja.component.css']
})
export class RejestracjaComponent implements OnInit {
    private login: string;
    private pass: string;
    private imie: string;
    private nazwisko: string;

    constructor(private rej: RejestracjaService) { }

  ngOnInit() {
  }

  rejestracja() {
      console.log(`login: ${this.login}`);
      console.log(`pass: ${this.pass}`);
      console.log(`imie: ${this.imie}`);
      console.log(`nazwisko: ${this.nazwisko}`);
      let uz = {
          imie: this.imie,
          nazwisko: this.nazwisko,
          email: this.login,
          password: this.pass
      }
      this.rej.rejestruj(uz);

  }

}
