import { Component, OnInit, HostListener } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { AdresService } from './../../../services/adres.service';
import { DodajDrukarkeService } from './../../../services/dodaj-drukarke.service';
import { Router } from '@angular/router';
import { LogowanieService } from './../../../services/logowanie.service';

declare var google: any;
@Component({
  selector: 'app-dodaj-drukarke',
  templateUrl: './dodaj-drukarke.component.html',
  styleUrls: ['./dodaj-drukarke.component.css']
})
export class DodajDrukarkeComponent implements OnInit {
    private nazwa: string;
    private lokalizacja: string;
    private szerokosc: string;
    private wysokosc: string;
    private zwrot: any[] = [];
    private tabo: any[] = [];
    private lokalizacjaChange: Subject<string> = new Subject<string>();
    private map: any;
    private koordynaty: any;
    private detale: boolean = false;
    private opis1: string;
    private opis2: string;

    private options: any = {
        zoom: 13,
        center: {
            lat: 52.1676031,
            lng: 22.2901645
        },
        mapTypeControlOptions: {
            mapTypeIds: []
        }, // here´s the array of controls
        disableDefaultUI: true, // a way to quickly hide all controls
        mapTypeControl: true,
        scaleControl: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE
        }
    };

    constructor(private as: AdresService, private ds: DodajDrukarkeService,
        private ls: LogowanieService, private router: Router) { }

    ngOnInit() {
      this.zalogowany();
      this.pokaMape();

      this.lokalizacjaChange
          .debounceTime(300)
          .distinctUntilChanged()
          .subscribe((ad) => {
              this.zwrot = [];
              this.as.szukaj(ad);
          });

      this.as.valu.subscribe(
          res => {
              let tab = [];
              tab = res.results;
            
              for (let n = 0; n < tab.length; n++) {
                  if (tab[n].formatted_address) {
                      this.zwrot[n] = 
                      {
                          adres: tab[n].formatted_address,
                          szerokosc: tab[n].geometry.location.lat,
                          wysokosc: tab[n].geometry.location.lng
                      }
                  }
              }
              
          }
      );
    }
    pokaDetale() {
        if (this.detale) {
          this.detale = false;
          if (window.innerWidth <= 580) {
            document.getElementById('lewe').style.display = 'block';
          }
        } else {
          this.detale = true;
          console.log("ZASZLO TRUE: " + window.screen.width);
          if (window.innerWidth <= 580) {
            document.getElementById('lewe').style.display = 'none';
          }
        }
    }
    @HostListener('window:resize',['$event'])
    onResize(event) {
      // console.log(`Wykryto resize: ${event.target.innerWidth}`);
      if (event.target.innerWidth <= 580 && this.detale) {
        
          console.log(`Przeszlo width: ${event.target.innerWidth} detale: ${this.detale}`)
          document.getElementById('lewe').style.display = 'none';
        } else {
          document.getElementById('lewe').style.display = 'block';
        }
      
    }
  propozycja(event) {
      this.lokalizacja = event.target.innerText;
      for (let n = 0; n < this.zwrot.length; n++) {
          if (this.zwrot[n].adres === event.target.innerText) {
              this.nowaPozycja(this.zwrot[n]);
          }
      }
      this.zwrot = [];
  }
  nowaPozycja(poz) {
      this.options.center.lat = poz.szerokosc;
      this.options.center.lng = poz.wysokosc;
      this.map = new google.maps.Map(document.getElementById('map'), this.options);
      let marker = new google.maps.Marker({
          position: {
              lat: poz.szerokosc,
              lng: poz.wysokosc
          },
          map: this.map
      });
      this.koordynaty = poz;
  }
  dodaj() {
      console.log('opis1: ');
      console.log(this.opis1);
      console.log('Koordynaty: ');
      console.log(this.koordynaty);
      let info = {
          nazwa: this.nazwa,
          szerokosc: this.koordynaty.szerokosc,
          wysokosc: this.koordynaty.wysokosc,
          opis: [
              {
                  tresc: this.opis1
              }, {
                  tresc: this.opis2
              }
              
          ],
          UzytkownikId: JSON.parse(localStorage.getItem('auth_token')).id,
          avatar: "http://68.media.tumblr.com/764a7a5b55c400887a366fc95327345a/tumblr_n2b3bzPfgI1r4xjo2o1_250.gif"

          
      }
      console.log("INFO przed wyslaniem");
      console.log(info);
      this.ds.dodaj(info).subscribe(
          res => {
              console.log("Respond: ");
              console.log(res);
          },
          err => console.log('error: ' + err)
          
      );
      
  }
  onLokalizacja(value: string) {
      this.lokalizacjaChange.next(value);
  }
  zalogowany() {
      this.ls.isLoggedIn().subscribe(
          res => console.log("Jest zalogowany"),
          err => this.router.navigate(['logo'])
      );
  }

  pokaMape() {
      
    
      this.map = new google.maps.Map(document.getElementById('map'), this.options);
      //here(options.center.lat, options.center.lng)
      //pinezki(options.center.lat, options.center.lng);
      //ds = new google.maps.DirectionsService();
      //dd = new google.maps.DirectionsRenderer();
  
  }
}
