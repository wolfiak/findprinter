import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DodajDrukarkeComponent } from './dodaj-drukarke.component';

describe('DodajDrukarkeComponent', () => {
  let component: DodajDrukarkeComponent;
  let fixture: ComponentFixture<DodajDrukarkeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DodajDrukarkeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DodajDrukarkeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
