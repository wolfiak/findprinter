import { Component, OnInit, HostListener } from '@angular/core';
import { WezDrukarkiService } from './../../../services/wez-drukarki.service';
import { Subject } from 'rxjs/Subject';
import { AdresService } from './../../../services/adres.service';
import { OpiniaService } from './../../../services/opinia.service';
import { TransakcjeService } from './../../../services/transakcje.service';
import { PaypalService } from './../../../services/paypal.service';
import { CzatService } from './../../../services/czat.service';
import { CzatProviderService } from './../../../services/czat-provider.service';
import { Router } from '@angular/router';

declare var google: any;
@Component({
  selector: 'app-wyniki',
  templateUrl: './wyniki.component.html',
  styleUrls: ['./wyniki.component.css']
})
export class WynikiComponent implements OnInit {
    private map: any;
    private tab: any;
    private lokalizacja: string;
    private pokaDetale: boolean = false;
    private tytul: string;
    private tmpId: number = 0;
    private adresChange: Subject<string> = new Subject<string>();
    private zwrot: string[] = [];
    private tabo: any[] = [];
    private tmpObiekt: any;
    private koordynaty: any;
    private odin: boolean = false;
    private dwa: boolean = false;
    private opis1: string;
    private opis2: string;
    private avatar: string;
    private tresc: string;
    private obiekt: any;
    private tablicaOpinii: any[];
    private tablica: boolean[] = [true, false, false];
    private thumbs: boolean[] = [false, false];
    private gora: number =0;
    private dol: number = 0;
    private wynikLapki: number;
    private plik: any;
    private wiadomosc;
    private iloscKopii;
    private piorytet;
    private piorytety: string[] = ["Na dziś", "Na jutro", "Na zaraz"];
    private format:string ='Nie ustalono';
    private formaty: string[] = ["A4", "A3", "A1"];
    private iloscStron: number=0;
    private cena: number=0.0;
    private options: any = {
        zoom: 13,
        center: {
            lat: 52.1676031,
            lng: 22.2901645
        },
        mapTypeControlOptions: {
            mapTypeIds: []
        }, // here´s the array of controls
        disableDefaultUI: true, // a way to quickly hide all controls
        mapTypeControl: true,
        scaleControl: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE
        }
    };

    constructor(private wds: WezDrukarkiService, private as: AdresService, private os: OpiniaService,
      private ts: TransakcjeService, private pps: PaypalService, private czs: CzatService, private router: Router, private czps: CzatProviderService) { }

    ngOnInit() {
        this.zmieniarka(0);
        this.adresChange.subscribe(
            res => {
                this.zwrot = [];
                this.as.szukaj(res);
            }
        );

        this.as.valu.subscribe(
            res => {
                this.tabo = res.results;
                for (let n = 0; n < this.tabo.length; n++) {
                    this.zwrot[n] = this.tabo[n].formatted_address;
                }
                console.log("Tablica zwrotow");
                console.log(this.zwrot);
                
            }
        );
      
      
        this.pokaMape();
        if (this.wds.getLokalizacja()) {
            this.wds.getTab().then((res) => {
                this.tab = res;
                this.formatowanieOdleglosci();
                this.marker();
            });
            this.lokalizacja = this.wds.getLokalizacja().lokalizacja;
            console.log('Lokalizacja: ');
            console.log(this.lokalizacja);
          
        } else {
            this.lokalizacja = "Siedlce";
        }
  }
  dane() {
      
    }
  zmianka(ktory) {
      for (let n = 0;n<this.tablica.length;n++){
          if (n == ktory) {
              this.tablica[n] = true;
          } else {
              this.tablica[n] = false;
          }
      }
  }
  upOrDown(ktory) {
      console.log("Klik");
      for (let n = 0; n < this.thumbs.length; n++) {
          if (n == ktory) { 
              console.log("Zaszlo true");
              this.thumbs[n] = true;
          } else {
              this.thumbs[n] = false;
          }
      }
  }
  propozycja(event) {
      this.lokalizacja = event.target.innerText;
      for (let n = 0; n < this.tabo.length; n++) {
          if (this.tabo[n].formatted_address == this.lokalizacja) {
              this.koordynaty = {
                  Szerokosc: this.tabo[n].geometry.location.lat,
                  Wysokosc: this.tabo[n].geometry.location.lng,
              }
          }

      }
      this.tabo = [];
      this.zwrot = [];
      this.wds.wez(this.koordynaty, this.lokalizacja);
      this.pokaMape();
      this.wds.getTab().then((res) => {
          console.log("ZWROT Z TEHN");
          console.log(res);
          this.tab = res;
          this.formatowanieOdleglosci();
          this.marker();

      });
      console.log("TAB PRZED MARKER: ");
      console.log(this.tab);
     

  }
  onTap(eve) {
      this.adresChange.next(eve);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth <= 900 && this.pokaDetale) {
      document.getElementById('lewe').style.display = 'none';
    } else {
      document.getElementById('lewe').style.display = 'block';
    }
  }
  poka(obiekt, event) {
    if (this.pokaDetale === true && (obiekt == null || obiekt.ID === this.tmpId )) {
         // console.log(`obiekt.id: ${obiekt.Id} this.tmpId: ${this.tmpId}`)
          this.pokaDetale = false;
          this.tmpId = null;
          
          if (window.innerWidth <= 900) {
            document.getElementById('lewe').style.display = 'block';
          }
          if (!event) {
            this.odznacz(this.tmpObiekt);
          }
      } else {
          if (this.pokaDetale === true && obiekt.ID != this.tmpId) {
              this.odznacz(this.tmpObiekt)
        }
          if (window.innerWidth <= 900) {
            document.getElementById('lewe').style.display = 'none';
          }
          this.tablicaOpinii = [];
          this.gora = 0;
          this.dol = 0;
          this.wynikLapki = 0;
          this.obiekt = obiekt;
          this.wezOpinie();
          this.pokaOpis(obiekt);
          console.log("Obiekt:");
          console.log(obiekt);
          this.tytul = obiekt.Nazwa;
          console.log("POka tytul:");
          console.log(this.tytul);
          this.tmpId = obiekt.ID;
          this.pokaDetale = true;
          this.oznaczony(event);
          this.tmpObiekt = event;
       
          
      }
  }
  pokaOpis(obiekt) {
      console.log("Dzialam z opisami");
      for (let n = 0; n < this.tab.length; n++) {
          if (this.tab[n].Xero.ID == obiekt.ID) {
              this.opis1 = this.tab[n].Xero.Opis[0].Tresc;
              this.opis2 = this.tab[n].Xero.Opis[1].Tresc;
              this.avatar = this.tab[n].Xero.avatar;
          }
      }

  }
  oznaczony(eve) {
      console.log("OZNACZONY:");
      console.log(eve);
     
      for (let n = 0; n < eve.path.length; n++) {
          if (eve.path[n].className == "kartki mat-card") {
              eve.path[n].className += " skart";
          }
      }
  }
  odznacz(eve) {
      console.log("Odznaczony:");
      console.log(eve);
      for (let n = 0; n < eve.path.length; n++) {
          if (eve.path[n].className == "kartki mat-card skart") {
              eve.path[n].className = "kartki mat-card";
          }
      }
    
  }
  marker() {
      console.log("Tablica struktura: ");
      console.log(this.tab);
      for (let n = 0; n < this.tab.length; n++) {
          this.mark(parseFloat(this.tab[n].Xero.Szerokosc), parseFloat(this.tab[n].Xero.Wysokosc));
      }
  }
  mark(lat, lng) {
      let mark = new google.maps.Marker({
          position: {
              lat: lat,
              lng: lng
          },
          map: this.map
      });
      let info = new google.maps.InfoWindow({
          content: 'Test'
      });
      mark.addListener('click', () => {
          info.open(this.map, mark);
      });
  }
  zmieniarka(licznik) {
     
      setInterval(() => {
          licznik++;
          if (licznik > 2) {
              licznik = 0;
          }

          if (licznik == 1) {
              this.dwa = false;
              this.odin = true;
          } else if (licznik == 2) {
              this.odin = false;
              this.dwa = true;
          }
      }, 5000);
  }
  pokaMape() {
      if (this.wds.getLokalizacja()){
      let kor = this.wds.getLokalizacja().koordynaty;
      console.log("Sprawdzam kor: ");
      console.log(kor);
      this.options.center.lat = kor.Szerokosc;
      this.options.center.lng = kor.Wysokosc;
      }

      this.map = new google.maps.Map(document.getElementById('map'), this.options);
      //here(options.center.lat, options.center.lng)
      //pinezki(options.center.lat, options.center.lng);
      //ds = new google.maps.DirectionsService();
      //dd = new google.maps.DirectionsRenderer();

  }
  change(eve) {
      console.log(eve);
  }
  wezOpinie() {
      console.log("sprawdzenie obiektu");
      console.log(this.obiekt);
      this.os.getOpinie(this.obiekt.ID).subscribe(res => {
          console.log("RESPOND:");
          console.log(res);
         
          this.os.getUz(res).subscribe(res => {
              console.log("Getu uz:");
              console.log(res);
              this.tablicaOpinii = res;
              this.ileLapek();
          });
      });
      
     
  }
  ileLapek() {
      this.gora = 0;
      this.dol = 0;
      for (let n = 0; n < this.tablicaOpinii.length; n++) {
          if (this.tablicaOpinii[n].thumb) {
              this.gora++;
          } else {
              this.dol++;
          }
      }
      let calosc: number = this.gora + this.dol;
       this.wynikLapki = (this.gora / calosc) *100;
       console.log("Niby procent: ");
       console.log(this.wynikLapki);
  }
  dodajOpinie() {
      let thum
      if (this.thumbs[0]) {
          thum = true;
      } else {
          thum = false;
      }
      let opinia = {
          tresc: this.tresc,
          thumb: thum,
          uzytkownikIdo: JSON.parse(localStorage.getItem('auth_token')).id,
          xeroIdo: this.obiekt.ID
      }
      console.log("TEST OPINIA");
      console.log(opinia);
      this.os.wyslij(opinia).subscribe(res => {
          console.log("RESPOND W WYNIK COMPONENT");
          console.log(res);
      });
  }
  formatowanieOdleglosci() {
      for (let n = 0; n < this.tab.length; n++) {     
          if (this.tab[n].odleglosc > 1000) {
              this.tab[n].odleglosc = this.tab[n].odleglosc / 1000;
              this.tab[n].odleglosc = Math.floor(this.tab[n].odleglosc).toString() + " km";
          } else {
              this.tab[n].odleglosc = Math.floor(this.tab[n].odleglosc).toString()+" m";
          }
         
          
      }
  }
  wyslijTransakcje() {
  
 
      console.log("SPRAWDZAM PLIK:");
      console.log(this.plik);
      this.ts.wyslijPlik(this.plik).subscribe(k => {
          console.log("Wynik");
          let combine = {
              Sciezka: k.path,
              UzytkownikIdo: JSON.stringify(JSON.parse(localStorage.getItem('auth_token')).id),
              XeroIdo: this.obiekt.ID,
              Format: this.format,
              IloscKopii: this.iloscKopii,
              Piorytet: this.piorytet.value,
              Wiadomosc: this.wiadomosc,
              PayPalId: "",
              Cena: this.cena,
              Ilosc: this.iloscKopii,
              StatusPlatnosci: "Nie zaplacono",
              Nazwa: "",
              Data: new Date()
          }
          console.log("Obiekt przed wyslaniem");
          console.log(combine);
          this.ts.stworzTransakcje(combine).subscribe(t => {
              console.log("Transakcja");
              console.log(t);
              //let ob = t.json();
             // console.log(ob.id);
              let obiekt = {
                obiekt: {
                  nazwa: this.tytul,
                  cena: this.cena,
                  ilosc: this.iloscKopii,
                  transakcjaId: t.id
            
                }
              }
              this.pps.wykonajPlatnosc(obiekt)
                .subscribe(res => {
                  console.log("Respond: ");
                  console.log(res);
                });
          });
      });
  }
  read(event) {
    this.plik = event;
    this.ts.wyslijDane(event)
      .subscribe(res => {
        console.log("ZWROT Z DANYCH");
        console.log(res);
        this.cena = Math.round(res.cena);
        this.iloscStron = res.iloscStron;
        this.format = res.format;
      });
  }
  onPiorytet(event) {
      console.log("Wykryto zmiane");
      console.log(event);
      this.piorytet = event;
  }
  onFormat(event) {
      console.log("Wykryto zmiane");
      console.log(event);
      this.format = event;
  }
  czat() {
    console.log("tMP DO modala");
    console.log(this.obiekt)
    this.czs.nowaRozmowa(this.obiekt.ID, JSON.parse(localStorage.getItem('auth_token')).id)
      .subscribe(res => {
        console.log("Klik czat");
        console.log(res);
        //this.router.navigate()
        this.czps.setID(res.id);
        this.router.navigateByUrl('/switch/(aux:wiadomosc)');
      });
  }
}
