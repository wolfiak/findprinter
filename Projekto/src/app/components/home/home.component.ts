import { Component, OnInit, OnDestroy } from '@angular/core';
import { Http } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { AdresService } from './../../../services/adres.service';
import { Router } from '@angular/router';
import { LogowanieService } from './../../../services/logowanie.service';
import { WezDrukarkiService } from './../../../services/wez-drukarki.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy { 
    private values: string[] = [];
    private zwrot: string[] = [];
    private tab: any[];
    private szukaj: string = "";
    private adresChange: Subject<string> = new Subject<string>();
    private koordynaty: any;
    private adres:string;

    constructor(private _http: Http, private _ad: AdresService, private router: Router
        , private ls: LogowanieService, private wds: WezDrukarkiService) { }

    ngOnDestroy() {
        this.adresChange.unsubscribe();
    }

    ngOnInit() {
       
        this.adresChange
            .subscribe((ad) => {
                this.zwrot = [];
                this._ad.szukaj(ad);
            });

        this._ad.valu.subscribe((res) => {
            console.log("ZWROT");
            console.log(res);
            this.tab = res.results;
            console.log("Tab ");
            // console.log(this.tab[0].formatted_address);
            for (let n = 0; n < this.tab.length; n++) {
                if (this.tab[n].formatted_address) {
                    this.zwrot[n] = this.tab[n].formatted_address;
                }
            }

        });
    }

    onTap(value: string) {
        this.adresChange.next(value);
    }
    focOut() {
        console.log("Zaszlo");

        setTimeout(() => {
            this.zwrot = [];
        }, 5000);
    }
    propozycja(event) {
        console.log("pROPOZYCJA");
        console.log(event);
        this.szukaj = event.target.innerText;
        for (let n = 0; n < this.tab.length; n++) {
            if (this.tab[n].formatted_address == this.szukaj) {
                this.koordynaty = {
                    Szerokosc: this.tab[n].geometry.location.lat,
                    Wysokosc: this.tab[n].geometry.location.lng
                }
            }
        }
        this.tab = [];
        this.zwrot = [];
        console.log("Wynik korydnatow:");
        console.log(this.koordynaty);
       
    }


  dodaj() {

      this.ls.isLoggedIn().subscribe(
          res => this.router.navigate(['dodajDrukarke']),
          err => this.router.navigate(['logo'])
       );
       
        
    }
    znajdz() {
        console.log("Adres:");
        console.log(this.adres);
        this.wds.wez(this.koordynaty, this.adres);
    }
}
