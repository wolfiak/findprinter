import { Component, OnInit } from '@angular/core';
import { Uzytkownik } from './../../../model/IUzytkownik';
import { LogowanieService } from './../../../services/logowanie.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-logowanie',
  templateUrl: './logowanie.component.html',
  styleUrls: ['./logowanie.component.css']
})
export class LogowanieComponent implements OnInit {
    login: string;
    pass: string;
    constructor(private ls: LogowanieService, private router: Router) { }

  ngOnInit() {
  }

  logowanie() {
      let uz = {
          imie: '',
          nazwisko: '',
          email: this.login,
          password: this.pass
      };
      this.ls.loguj(uz).subscribe(
          res => {
            if (res) {
              console.log("Logowanie poszlo");
                  this.router.navigate(['']);
              }
          },
          err => console.log(err)

      );
  }
}
