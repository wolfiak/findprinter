import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AdresService } from '../services/adres.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { RouterModule, Routes } from '@angular/router';

import { LogowanieComponent } from './components/logowanie/logowanie.component';

import { RejestracjaComponent } from './components/rejestracja/rejestracja.component';

import { MatInputModule } from '@angular/material';
import { MatCardModule } from '@angular/material';
import { MatButtonModule } from '@angular/material';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSelectModule } from '@angular/material/select';


import { RejestracjaService } from './../services/rejestracja.service';
import { LogowanieService } from './../services/logowanie.service';
import { DodajDrukarkeComponent } from './components/dodaj-drukarke/dodaj-drukarke.component';

import { DodajDrukarkeService } from './../services/dodaj-drukarke.service';
import { CzatProviderService } from './../services/czat-provider.service';
import { PaypalService } from './../services/paypal.service';
import { CzatService } from './../services/czat.service';
import { OpiniaService } from './../services/opinia.service';
import { WezDrukarkiService } from './../services/wez-drukarki.service';
import { TransakcjeService } from './../services/transakcje.service';
import { WynikiComponent } from './components/wyniki/wyniki.component';

import { WiadomosciComponent } from './components/wiadomosci/wiadomosci.component';
import { SwitchComponent } from './components/switch/switch.component';
import { NawigacjaComponent } from './components/nawigacja/nawigacja.component';


const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'switch', component: SwitchComponent, children: [
      { path: 'wiadomosc', component: WiadomosciComponent, outlet: 'aux' },
     
      { path: 'wynik', component: WynikiComponent, outlet: 'aux' }
    ]},
    { path: 'logo', component: LogowanieComponent },
    { path: 'rejestracja', component: RejestracjaComponent },
    { path: 'dodajDrukarke', component: DodajDrukarkeComponent},
    { path: 'wiadomosc', component: WiadomosciComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LogowanieComponent,
    RejestracjaComponent,
    DodajDrukarkeComponent,
    WynikiComponent,
    WiadomosciComponent,
    SwitchComponent,
    NawigacjaComponent
  ],
  imports: [
      BrowserModule,
      FormsModule,
      HttpModule,
      BrowserAnimationsModule,
      MatInputModule,
      MatCardModule,
    MatButtonModule,
    MatProgressBarModule,
    MatSelectModule,
      RouterModule.forRoot(
          appRoutes
      )
  ],
  providers: [RejestracjaService, AdresService, LogowanieService, DodajDrukarkeService
    , WezDrukarkiService, OpiniaService, TransakcjeService, CzatService, PaypalService, CzatProviderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
