using FindPrinter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.ToolBox
{
    public  class DataTools
    {
    public  EventHandler<Transakcje> ticketWygasl;
    public EventHandler<Transakcje> naDzis;
    public EventHandler<Transakcje> naZaraz;
    private  void onTicketWygasl(Transakcje t)
    {
      if (ticketWygasl != null)
      {
        ticketWygasl(this, t);
      }
    }
    private void onNaDzis(Transakcje t)
    {
      naDzis?.Invoke(this, t);
    }
    private void onZaraz(Transakcje t)
    {
      if (naZaraz != null)
      {
        naZaraz(this, t);
      }
    }
     public  void porownaj(int wynik, Transakcje t)
    {
      Console.WriteLine("Porownywaniee wynik:"+wynik);
      if (wynik < 0)
      {
        onTicketWygasl(t);
      }
      else if (wynik == 0)
      {

        onNaDzis(t);

      } else if (wynik == 2) {
           onZaraz(t);
      } else {

      }
    }
    
    }
}
