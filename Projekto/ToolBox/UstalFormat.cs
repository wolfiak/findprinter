
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.ToolBox
{
    public static class UstalFormat
    {

       public static string ustalFormat(this PdfReader reader)
       {
         int szerokosc = (int) Math.Round(reader.GetPageSizeWithRotation(1).Width);
         int wysokosc = (int)Math.Round(reader.GetPageSizeWithRotation(1).Height);

         int s = szerokosc - (int)PageSize.A4.Width;
         int w = wysokosc - (int)PageSize.A4.Height;

         if(s == 0 && w== 0)
         {
           return "A4";
         }
         if(s >=-2 && s <= 2)
         {
           return "A4";
         }
         else
         {
           return "blad";
         }

       }
        
  }

}
