using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekto.ToolBox
{
    public static class DataPorownanie
    {
      public static int naPoziomieDni(DateTime data1, DateTime data2)
    {
      if(data1.Date == data2.Date)
      {
        int godzina = data2.Millisecond- data1.Millisecond;
        if(godzina <= 3600000 && godzina >0)
        {
          return 2;
        }
        return 0;
      }else if(data1.Date < data2.Date)
      {
        return -1;
      }
      else
      {
        return 1;
      }
    }
    public static int naPoziomieGodzin(DateTime data1, DateTime data2)
    {
      if (data1.Millisecond == data2.Millisecond)
      {
        return -1;
      }
      else if (data1.Millisecond < data2.Millisecond)
      {
        return -1;
      }
      else
      {
        return 1;
      }
    }
    }
}
